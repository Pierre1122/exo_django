from rest_framework import serializers

from map.models import Layer
from map.models import Field


class LayerSerializer(serializers.ModelSerializer):
    """
    Serializer for Layer
    """
    class Meta:
        model = Layer
        fields = ('slug', 'display_name', 'table_name',
                  'root_object', 'id_field_name', 'geom_geo_field_name',
                  'geom_sch_field_name', 'fields')


class FieldSerializer(serializers.ModelSerializer):
    """
    Serializer for Field
    """
    class Meta:
        model = Field
        fields = ('display_name',
                  'field_name',
                  'is_in_root')
