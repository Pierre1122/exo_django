from django.http.response import JsonResponse

from rest_framework.parsers import JSONParser
from rest_framework import status
from rest_framework.decorators import api_view

from map.models import Field
from map.serializers import FieldSerializer
from map.models import Layer
from map.serializers import LayerSerializer


@api_view(['GET', 'POST', 'DELETE'])
def field_list(request):
    """
    GET: list all fields
    POST: create a field
    DELETE: delete all fields
    """
    if request.method == 'GET':
        field = Field.objects.all()
        field_serializer = FieldSerializer(field, many=True)
        return JsonResponse(field_serializer.data, safe=False)

    elif request.method == 'POST':
        field_data = JSONParser().parse(request)
        field_serializer = FieldSerializer(data=field_data)
        if field_serializer.is_valid():
            field_serializer.save()
            return JsonResponse(field_serializer.data, status=status.HTTP_201_CREATED)
        return JsonResponse(field_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        count = Field.objects.all().delete()
        return JsonResponse({'message': '{} Fields were deleted successfully!'.format(count[0])},
                            status=status.HTTP_204_NO_CONTENT)


@api_view(['GET', 'POST', 'DELETE'])
def layer_list(request):
    """
    GET: list all layers
    POST: create a layer
    DELETE: delete all layers
    """
    if request.method == 'GET':
        layers = Layer.objects.all()
        layers_serializer = LayerSerializer(layers, many=True)
        return JsonResponse(layers_serializer.data, safe=False)

    elif request.method == 'POST':
        layer_data = JSONParser().parse(request)
        layer_serializer = LayerSerializer(data=layer_data)
        if layer_serializer.is_valid():
            layer_serializer.save()
            return JsonResponse(layer_serializer.data, status=status.HTTP_201_CREATED)
        return JsonResponse(layer_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        count = Layer.objects.all().delete()
        return JsonResponse({'message': '{} Layers were deleted successfully!'.format(count[0])},
                            status=status.HTTP_204_NO_CONTENT)


@api_view(['GET', 'PUT', 'DELETE'])
def field_detail(request, pk):
    """
    GET: list a field
    PUT: retrieve the field corresponding to the given pk
    DELETE: delete the field corresponding to the given pk
    """
    try:
        field = Field.objects.get(pk=pk)
    except Field.DoesNotExist:
        return JsonResponse({'message': "id doesn't exist"}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        field_serializer = FieldSerializer(field)
        return JsonResponse(field_serializer.data)

    elif request.method == 'PUT':
        field_serializer = FieldSerializer(field, data=request.data)
        if field_serializer.is_valid():
            field_serializer.save()
            return JsonResponse(field_serializer.data)
        return JsonResponse(field_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        field.delete()
        return JsonResponse({'message': 'Field was deleted successfully!'},
                            status=status.HTTP_204_NO_CONTENT)


@api_view(['GET', 'PUT', 'DELETE'])
def layer_detail(request, pk):
    """
    GET: list a layer
    PUT: retrieve the layer corresponding to the given pk
    DELETE: delete the layer corresponding to the given pk
    """
    try:
        layer = Layer.objects.get(pk=pk)
    except Layer.DoesNotExist:
        return JsonResponse({'message': "id doesn't exist"},
                            status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        layer_serializer = LayerSerializer(layer)
        return JsonResponse(layer_serializer.data)

    elif request.method == 'PUT':
        layer_serializer = LayerSerializer(layer, data=request.data)
        if layer_serializer.is_valid():
            layer_serializer.save()
            return JsonResponse(layer_serializer.data)
        return JsonResponse(layer_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        layer.delete()
        return JsonResponse({'message': 'Layer was deleted successfully!'},
                            status=status.HTTP_204_NO_CONTENT)
