from django.conf.urls import url
from map import views

urlpatterns = [ 
    url(r'^api/fields$', views.field_list),
    url(r'^api/layers$', views.layer_list),
    url(r'^api/fields/(?P<pk>[0-9]+)$', views.field_detail),
    url(r'^api/layers/(?P<pk>[0-9]+)$', views.layer_detail),
]