from django.contrib import admin

from map.models import Layer, Field


@admin.register(Layer)
class LayerModelAdmin(admin.ModelAdmin):
    pass


@admin.register(Field)
class FieldModelAdmin(admin.ModelAdmin):
    pass

