from django.contrib.contenttypes.models import ContentType
from django.db import models


class Layer(models.Model):
    slug = models.SlugField(unique=True)
    display_name = models.CharField(max_length=120)
    table_name = models.CharField(max_length=240)
    root_object = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    id_field_name = models.CharField(max_length=120)
    geom_geo_field_name = models.CharField(max_length=120, null=True, blank=True)
    geom_sch_field_name = models.CharField(max_length=120, null=True, blank=True)
    fields = models.ManyToManyField('Field', blank=True)

    def __str__(self):
        return self.display_name

    class Meta:
        constraints = [
            models.CheckConstraint(check=models.Q(geom_geo_field_name__isnull=False)
                                   | models.Q(geom_sch_field_name__isnull=False), name='has_geom_field_name'),
        ]


class Field(models.Model):
    display_name = models.CharField(max_length=120)
    field_name = models.CharField(max_length=120)
    is_in_root = models.BooleanField()

    def __str__(self):
        return self.display_name

