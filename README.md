# Exercice Django

Le but de cet exercice est de corriger l'application afin de pouvoir ajouter des `Layer` ou `Field` depuis l'admin
Django. Puis de créer une API REST CRUD à l'aide de `Django REST framework`.

De plus vous devez utiliser `Postgresql` avec le dump `exo.backup`.
Si vous effectuez des modifications du dump original vous devrez les lister dans un fichier expliquant ces changements.

Le code doit être :
  - propre, concis et respecter la norme `PEP8`
  - rédigé en anglais
  - documenté
  
Une fois le travail réalisé, vous devrez le mettre à disposition via une `Merge Request` sur ce répertoire.

**Bonne chance !**

# Notes (Pierre)

J'ai eu accès au projet lundi midi et j'ai du configurer mon PC car je n'avais jamais fait de projet dessus.

Une fois la base de données restaurée (grâce au dump exo.backup), il faut runner les commandes de script.sql dans la base de données. Cela va ajouter tout ce qui est nécessaire pour que le clés primaires des tables map_layer et map_field s'auto-incrémentent.

A noter que ce code est une version simple. On pourrait rajouter plus de fonctionnalités, comme par exemple l'authentification.
